#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui.hpp>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cusolverDn.h>
#include <cub/cub.cuh>

using namespace std;
using namespace cv;

struct CustomMin
{
    template <typename T>
    __device__ __forceinline__
    T operator()(const T &a, const T &b) const {
        return (b < a) ? b : a;
    }
};

static void read_csv(const string& filename, vector<Mat>& images, vector<int>& labels, char separator = ';') {
	std::ifstream file(filename.c_str(), ifstream::in);
	if (!file) {
		string error_message = "No valid input file was given, please check the given filename.";
		CV_Error(CV_StsBadArg, error_message);
	}
	string line, path, classlabel;
	while (getline(file, line)) {
		stringstream liness(line);
		getline(liness, path, separator);
		getline(liness, classlabel);
		if(!path.empty() && !classlabel.empty()) {
			images.push_back(imread(path, 0));
			labels.push_back(atoi(classlabel.c_str()));
		}
	}
}

void printMatrix(int m, int n, const double*A, int lda, const char* name)
{
    for(int row = 0 ; row < m ; row++){
        for(int col = 0 ; col < n ; col++){
            double Areg = A[row + col*lda];
            printf("%s(%d,%d) = %f\n", name, row+1, col+1, Areg);
        }
    }
}

Mat getEigen(double * C)
{
	cusolverDnHandle_t cusolverH;
    cusolverStatus_t cusolver_status = CUSOLVER_STATUS_SUCCESS;
    cudaError_t	cudaStat1 = cudaSuccess;
    cudaError_t	cudaStat2 = cudaSuccess;
    cudaError_t	cudaStat3 = cudaSuccess;
    
	const int	m = 10;
    const int	lda = m;

	double	*	A = C;	//Dot product�� ����

    double	*	V = new double[lda*m];	// eigenvectors
    double	*	W = new double[m];		// eigenvalues

    double  *	d_A = NULL;
    double  *	d_W = NULL;
    int		*	devInfo = NULL;
    double	*	d_work = NULL;
    int			lwork = 0;
    int			info_gpu = 0;

    //printMatrix(m, m, C, lda, "matC");
    //printf("=====\n");

// eigen_step 1: create cusolver/cublas handle
    cusolver_status = cusolverDnCreate(&cusolverH);
    assert(CUSOLVER_STATUS_SUCCESS == cusolver_status);

// eigen_step 2: copy A and B to device
    cudaStat1 = cudaMalloc ((void**)&d_A, sizeof(double) * lda * m);
    cudaStat2 = cudaMalloc ((void**)&d_W, sizeof(double) * m);
    cudaStat3 = cudaMalloc ((void**)&devInfo, sizeof(int));
    assert(cudaSuccess == cudaStat1);
    assert(cudaSuccess == cudaStat2);
    assert(cudaSuccess == cudaStat3);

    cudaStat1 = cudaMemcpy(d_A, A, sizeof(double) * lda * m, cudaMemcpyHostToDevice);
    assert(cudaSuccess == cudaStat1);

// eigen_step 3: query working space of syevd
    cusolverEigMode_t jobz = CUSOLVER_EIG_MODE_VECTOR; // compute eigenvalues and eigenvectors.
    cublasFillMode_t uplo = CUBLAS_FILL_MODE_LOWER;
    cusolver_status = cusolverDnDsyevd_bufferSize(cusolverH, jobz, uplo, m, d_A, lda, d_W, &lwork);
    assert (cusolver_status == CUSOLVER_STATUS_SUCCESS);

    cudaStat1 = cudaMalloc((void**)&d_work, sizeof(double)*lwork);
    assert(cudaSuccess == cudaStat1);

// eigen_step 4: compute spectrum
    cusolver_status = cusolverDnDsyevd(cusolverH, jobz, uplo, m, d_A, lda, d_W, d_work, lwork, devInfo);
    cudaStat1 = cudaDeviceSynchronize();
    assert(CUSOLVER_STATUS_SUCCESS == cusolver_status);
    assert(cudaSuccess == cudaStat1);

    cudaStat1 = cudaMemcpy(W, d_W, sizeof(double)*m, cudaMemcpyDeviceToHost);
    cudaStat2 = cudaMemcpy(V, d_A, sizeof(double)*lda*m, cudaMemcpyDeviceToHost);
    cudaStat3 = cudaMemcpy(&info_gpu, devInfo, sizeof(int), cudaMemcpyDeviceToHost);
    assert(cudaSuccess == cudaStat1);
    assert(cudaSuccess == cudaStat2);
    assert(cudaSuccess == cudaStat3);

	printf("after syevd: info_gpu = %d\n", info_gpu);
    assert(0 == info_gpu);

    //printf("eigenvalue = (matlab base-1), ascending order\n");
    //for(int i = 0 ; i < m ; i++){
        //printf("W[%d] = %E\n", i+1, W[i]);
    //}

   // printMatrix(m, m, V, lda, "V");
   // printf("=====\n");

	vector<double>vv;
	float val;
	for(int i=0; i<m*lda; i++)
	{
		if(V[i] == 0.0)
			val == 0.0f;
		
		else
			val = V[i]/abs(V[i]);
		
		vv.push_back(val);
		//cout << vv[i] << endl;
	}
	Mat eigV = Mat(vv).reshape(1,m);

	//normalize(eigV, eigV, 0, 1, CV_MINMAX);
	//namedWindow( "Display window", WINDOW_AUTOSIZE );
	//imshow( "Display window", eigV); 
	//waitKey(0);

	return eigV.clone();
}

int main(int argc, char*argv[])
{
	string			git_dir(getenv("GITDIR"));
	git_dir += "/att_faces/output3.csv";

	string			fn_csv = string(git_dir);
	vector<Mat>		images;
	vector<int>		labels;


///	step1 : Read Images
	try {
		read_csv(fn_csv, images, labels);
	} catch (cv::Exception& e) {
		cerr << "Error opening file \"" << fn_csv << "\". Reason: " << e.msg << endl;
		exit(1);
	}

	if(images.size() <= 1) {
		string error_message = "This demo needs at least 2 images to work. Please add more images to your data set!";
		CV_Error(CV_StsError, error_message);
	}	


///	step3 : Average face vector	
	int mSize = images.size();
	Mat img32f;
	vector<Mat> vimg32f(images.size());
	Mat meanVector = Mat::zeros(images[0].rows, images[0].cols, CV_64F);
	for(int i=0; i<mSize; i++)
	{
		images[i].convertTo(img32f, CV_64F);
		meanVector += img32f;
	}
	meanVector *= (1.0/mSize);

	//normalize(meanVector, meanVector, 0, 1, CV_MINMAX);
	//namedWindow( "Display window", WINDOW_AUTOSIZE );
	//imshow( "Display window", meanVector); 
	//waitKey(0);


///	step4 : Subtract the mean face
	vector<Mat>		meanFaces;
	Mat sub, img32f2;
	
	for(int i=0; i<mSize; i++)
	{
		images[i].convertTo(img32f2, CV_64F);
		sub = img32f2 - meanVector;
		meanFaces.push_back(sub);

		//normalize(meanFaces[i], meanFaces[i], 0, 1, CV_MINMAX);
		//namedWindow( "Display window", WINDOW_AUTOSIZE );
		//imshow( "Display window", meanFaces[i]); 
		//waitKey(0);
	}


///	step2 : Vectorize the training image set
	Mat vec;
	vector<Mat>		vMeanFaces;
	for(int i=0; i<mSize; i++)
	{
		vec = meanFaces[i].reshape(1,images[i].cols*images[i].rows);
		vMeanFaces.push_back(vec);

		//namedWindow( "Display window", WINDOW_AUTOSIZE );
		//imshow( "Display window", vMeanFaces[i]); 
		//waitKey(0);
	}


///	step5 : Compute the covariance matrix
	double * C = new double[mSize*mSize];
	vector<double> C1;
	for(int i=0; i<mSize; i++)
	{
		for(int j=0; j<mSize; j++)
		{
			C[i*mSize+j] = vMeanFaces[i].dot(vMeanFaces[j]);
			C1.push_back(C[i*mSize+j]);

		}
	}
	Mat matC = Mat(C1).reshape(1,mSize);
	matC *= (1.0/mSize);


/// step6 : Eigenvectors && Representing faces onto this basis
	Mat eigVec = getEigen(C);
	//cout << eigVec << endl;

	Mat MatmeanFace, faceWeight;

	for(int i=0; i<mSize; i++)
	{
		MatmeanFace.push_back(vMeanFaces[i]);
	}

	MatmeanFace = MatmeanFace.reshape(1, vMeanFaces[0].rows);
	Mat newEigVec = MatmeanFace *  eigVec;

	for(int i=0; i<mSize; i++)
	{
		for(int j=0; j<eigVec.cols; j++)
		{
			faceWeight.push_back(newEigVec.col(j).dot(MatmeanFace.col(i)));
		}	
	}
	faceWeight = faceWeight.reshape(1, mSize);

	/*Mat testFace = Mat::zeros(MatmeanFace.rows, 1, CV_64F);
	for(int i=0; i< mSize; i++)
	{
		testFace += newEigVec.col(i) * faceWeight.col(0).row(i);
	}
	testFace = testFace.reshape(1, images[0].rows);
	normalize(testFace,testFace, 0, 1, CV_MINMAX);
	namedWindow( "Display window", WINDOW_AUTOSIZE );
	imshow( "Display window", testFace); 
	waitKey(0);*/


/// step7 : Face Recognition & Detection Using Eigenfaces
	Mat newImg32f;
	Mat newImg = images[0];	//imread("newimg.pgm");
	newImg.convertTo(newImg32f, CV_64F);

	Mat newNface = newImg32f - meanVector;
	newNface = newNface.reshape(1, newImg32f.cols*newImg32f.rows);
	
	Mat newFweight;
	for(int i=0; i<mSize; i++)
		newFweight.push_back(newEigVec.col(i).dot(newNface));


	// Recognition Omega == newFweight
	double * subWeight = new double[mSize];
	for(int i=0; i<mSize; i++)
		subWeight[i] = Mahalanobis(newFweight, faceWeight.col(i), matC);

	CustomMin			min_op;
	float				init = FLT_MAX;
	void		*		d_temp_storage = NULL;
	size_t				temp_storage_bytes = 0;
	double		*		d_subWeight;
	double		*		d_outDist;
	double				h_outDist;

	cudaMalloc(&d_subWeight, sizeof(double) * mSize);
	cudaMalloc(&d_outDist, sizeof(double));
	cudaMemcpy(d_subWeight, subWeight, sizeof(double)*mSize, cudaMemcpyHostToDevice);
	
	cub::DeviceReduce::Reduce(d_temp_storage, temp_storage_bytes, d_subWeight, d_outDist, mSize, min_op, init);
	cudaMalloc(&d_temp_storage, temp_storage_bytes);
	cub::DeviceReduce::Reduce(d_temp_storage, temp_storage_bytes, d_subWeight, d_outDist, mSize, min_op, init);

	cudaMemcpy(&h_outDist, d_outDist, sizeof(double), cudaMemcpyDeviceToHost);

	cout << h_outDist << endl;
	float Tr = FLT_MAX;

	if(h_outDist < Tr)
		cout << "newImage is within the face catagories" << endl;
	else
		cout << "newImage isn't within the face catagories" << endl;
	

	// Detection 
	Mat newLincom = Mat::zeros(newNface.rows, newNface.cols, CV_64F);
	for(int i=0; i<mSize; i++)
		newLincom += newEigVec.col(i) * newFweight.row(i);
	
	double Ed = norm(newNface - newLincom);
	cout << Ed << endl;
	
	float Td = FLT_MAX;

	if(Ed < Td)
		cout << "newImage is within the face space" << endl;
	else
		cout << "newImage isn't within the face space" << endl;
	

	/*
	//string git_dir(getenv("GITDIR"));
	//git_dir += "C:/git/TrainingSet_ImageTrainingSet_Image/";
	
    
	string filename;
	Mat * img = new Mat[NUMIMG];
	vector<float> * vImgs = new vector<float>[NUMIMG];
	vector<float>	vMean;
	float			fMean = 0;
	Mat vec;
	
	for(int i=0; i<NUMIMG; i++)
	{
		filename = format("C:/git/TrainingSet_Image/%d.jpg",i+1);
		img[i] = imread(filename, CV_LOAD_IMAGE_COLOR);

		if (img[i].empty())
		{ 
			printf("can NOT see any image!\n");
			return -1;
		}

		if(img[i].isContinuous())
		{
			//vec = img[i].reshape(0,1);
			//printf("%d\n",vec.size);
			//vImgs[i].insert((float)vec.data);
			//vec = img[i].reshape(0,1);

			vImgs[i].assign((float*)img[i].datastart, (float*)img[i].dataend);
			vec = img[i].reshape(0,1);
			//vImgs[i].reserve(vec.cols);
			//memcpy(&vImgs[i][0], vec.data, vec.cols*sizeof(float));
		}
		else
		{
			printf("img[%d] is NOT continuous!!!\n",i);
			return -1;
		}
	}

	for(int j=0; j<img[0].cols * img[0].rows; j++)
	{
		for(int k = 0; k<NUMIMG; k++)
		{
			fMean += vImgs[k].at(j);

		}
		vMean.push_back(fMean/NUMIMG);
	}
	
	namedWindow("EXAMPLE01", CV_WINDOW_AUTOSIZE);
	imshow("EXAMPLE01", img[1]);
    
	waitKey(0);
    destroyWindow("EXAMPLE01");
	*/
	return 0;
}